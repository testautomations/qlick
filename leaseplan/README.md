The Test automation framework was built using Maven Project and Java as programming language and Cucumber as automation framework with Serenity.

Installation you will need the following:

1. JAVA SE/JDK - version 15.0.1
   https://www.oracle.com/java/technologies/javase-downloads.html

2. MAVEN - version 3.8.1 
   https://maven.apache.org/download.cgi
   - Add new System Environment Variable with name “MAVEN_HOME” set value to the path where you extracted the maven folder until the "bin"

3. Eclipse or IntelliJ IDEA (I used eclipse for this project)
   https://www.eclipse.org/
   https://www.jetbrains.com/idea/

4. Install Cucumber Eclipse plugin or IntelliJ IDEA Cucumber for Java plugin.
   https://github.com/cucumber/cucumber-eclipse/blob/main/README.md
   https://plugins.jetbrains.com/plugin/7212-cucumber-for-java 

5. Once all necessary tools are installed, you may now import your project into your workspace using Eclipse or Intellij.
   And make sure that you can build the project without errors.

Tests Executions:
 Note: Feature File is consists of mix positive and negative tests

1. You can execute the tests using maven cli commands by opening a terminal or command prompt and making sure that you are in the current project folder 
   name "LeasePlan/" then type "mvn verify" this will execute all the tests as well as generate the reports
  
2. Here are the list of the tags available:
   1. To run all the tests as one feature
      - @feature
   2. To run all positive tests per tag scenarios:
      - @positive
   3. To run all negative tests per tag scenarios:
      - @negative  

3. I have created batch file to easily run the tests and you'll be able to pass tags
   Sample command: 
    > C:\LeasePlan>run\test.bat @positive

4. The test results are saved in the project folder under "target\site\serenity"
   You may want to view the overview report using the "index.html".

GitLab:
 1. All codes are all in the gitlab repository
 
 2. It is also integrated with CI Pipelines, these will be triggered everytime there will be a change in the repository
    - trigger will be every "git push"
	
 3. The Pipeline will execute the tests and create reports of the result.
 
 4. You can view the result from the pipeline job then on the right part you will see a "Browse" button
	- browse the folder to "LeasePlan/target/site/serenity/" folder then open the "index.html" using your favorite web browser

 5. Currently I'm only using my local runner in running the CI pipeline jobs, since I'm only using a trial version does not support "Shared"    

 
What was refactored:
 1. Added new class name: SearchAPI.java 
	- this is used as the model where all API Transactions and data manipulations are created
	
 2. Added new class name: SearchResponse
    - this is used to store all GPATH variables used by the SearchAPI Transactions
	
 3. RestAssured validation was changed to "AssertTrue", since we want that all transactions will be in the model page
	- the validations are now easily read and understandable since it will be directly invoking the "assertTrue" and calling the functions from SearchAPI.
	
 4. Used of "Scenario Outline" instead of "Scenario" to be able to test all the available test data even an error occurs the next tests will still be executed.
 
 5. Kept all global variable under "serenity.properties" and removed unnecessary configs under "serenity.conf" file since the automation is not intended for web testing
 
 6. Update "logback-test.xml" to create a log file under "log" folder with name "app.log", it is also updated to archive if it reached the size of 10MB
 
 7. Updated "pom.xml" to remove webdriver.base.url properties since we already set that in the "serenity.properties"
 
 How to create new tests:
 1. Make sure to add a new feature file under folder "/src/test/resources/features/" and name your file  with extension ".feature"
    - you can also directly add new tests in the existing feature file "post_product.feature"
	- make sure that you're familiar with Gherkin language to add new tests
	
 2. Create a new file for your "step definitions" following the naming convention of adding "StepDefinitions" at the end of your file sample: "SearchStepDefinitions.java"
    - all Gherkin steps will then be added here and for creating functions/methods must follow the naming convention of camelCasing
	
 3. Create new file for your API transactions if these is for a different endpoint fo example "Purchases" you have to name your file with naming convention by adding "API" at the end of the file sample: "PurchaseAPI.java"
    - all necessary endpoint or api transactions and data manipulations will added here
	
 4. Create new file for your GPATH or JSONPATH and should follow the naming convention by adding "Response" at the end of the file sample: "PurchaseResponse.java"	
    - gpaths are used as static variables so you can call them directly to your model class
 
Execution and Reports viewing:
 I have added videos of recorded executions and navigate to the reports:
  1. Triggered by "git push" 
     - GitLabCI_tests.mp4  
  2. Manual execution in Eclipse terminal
     - MavenCLI_tests.mp4
	 
 
 
	
   
   