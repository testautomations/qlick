package com.leaseplan.search;

public class SearchResponse {
	 public static final String SUCCESS = "200";
	 public static final String  NO_RESULT = "404";
	 
     public static final String  ERROR = "detail.error";
     public static final boolean  ERROR_RESULT = true;
     
     public static final String MESSAGE = "detail.message";
     public static final String MESSAGE_RESULT = "Not found";
     
     public static final String TITLE = "title";
}
