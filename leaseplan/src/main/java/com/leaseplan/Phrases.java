package com.leaseplan;

import net.serenitybdd.screenplay.Actor;

public class Phrases {
	
	private static Actor activeActor = null;
	
	public static Actor givenThat(Actor actor) {
		return activeActor = actor;
	}
	
	public static Actor getActiveActor() {
		return activeActor;
	}
	
	public static Actor andWhen(Actor actor) {
		return actor;
	}
	
	public static Actor and(Actor actor) {
		return actor;
	}
	
	public static Actor andHe() {
		return getActiveActor();
	}
	
	public static Actor andShe() {
		return getActiveActor();
	}
	
	public static Actor thenHe() {
		return getActiveActor();
	}

	public static Actor thenShe() {
		return getActiveActor();
	}
	
}
