package com.leaseplan.users;

import com.leaseplan.User;

import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;

public class Users  {
	private static EnvironmentVariables environmentVariables = SystemEnvironmentVariables.createEnvironmentVariables();
	private static String apiBaseUrl =  EnvironmentSpecificConfiguration.from(environmentVariables).getProperty("webdriver.base.url");	
	
	public Actor jordanKim = User.named("jordan kim jubilo").withEmail("jordan.kim.jubilo@gmail.com").usingPronoun("He").whoCan(CallAnApi.at(apiBaseUrl));;

}
