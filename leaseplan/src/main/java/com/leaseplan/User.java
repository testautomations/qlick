package com.leaseplan;

import net.serenitybdd.screenplay.Actor;

public class User extends Actor  {
	
	@SuppressWarnings("unused")
	private String username;
		
	@SuppressWarnings("unused")
	private String email;
	
	public User(String name) {
		super(name);
	}
	
	public static User named(String name) {
		return new User(name);
	}	
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getUsername() {
		return username;
	}
	
	public String getEmail() {
		return email;
	}
	
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	
	public User withEmail(String email) {
		this.setEmail(email);
		return this;
	}
	
	
	

}
