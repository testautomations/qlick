package com.leaseplan.question;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.serenitybdd.screenplay.Question;

public abstract class QuestionBase<T> implements Question<T> {
	protected Logger logger = LoggerFactory.getLogger(this.getClass());	

}
