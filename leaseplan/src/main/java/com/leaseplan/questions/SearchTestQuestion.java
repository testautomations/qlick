package com.leaseplan.questions;

import java.util.List;

import com.leaseplan.ResponseProperties;
import com.leaseplan.question.QuestionBase;
import com.leaseplan.search.SearchResponse;

import io.restassured.response.Response;
import net.serenitybdd.screenplay.Actor;


public class SearchTestQuestion extends QuestionBase<Boolean>{
	private String value;
	private  Response responseBody;
	private ResponseProperties responseProperty;
	
	
	
	public SearchTestQuestion(String valueOf, Response responseBody, ResponseProperties responseProperty) {
		this.value = valueOf;
		this.responseBody = responseBody;
		this.responseProperty = responseProperty;
	}

	@Override
	public Boolean answeredBy(Actor actor) {
		boolean foundIt = false;
		
		switch(responseProperty) {
			case TITLE:
				List<String> titles = responseBody.path(SearchResponse.TITLE);			    	
			        
		    	// check title contains the product
		        for (String title : titles)
		        {
		            if(title.toLowerCase().contains(value.toLowerCase())) {
		            	foundIt = true;
		            	break;
		            }
		        }
				break;
			case STATUS_CODE:
				if(responseBody.statusCode() == Integer.parseInt(value)) {
					foundIt = true;
		    	}  
				break;
		
			case ERROR:
				if(responseBody.path(SearchResponse.ERROR).equals(Boolean.parseBoolean(value))) {
					foundIt = true;
				}				
				break;
				
			case MESSAGE:
				if(responseBody.path(SearchResponse.MESSAGE).equals(value)) {
					foundIt = true;
		    	}    
				break;			
				
			default:break;	
		}
		
		
		return foundIt;
	}
	
	public static SearchTestQuestion shouldHaveTitleOf(String value, Response responseBody) {
		return new SearchTestQuestion(value, responseBody,ResponseProperties.TITLE);
	}
	
	public static SearchTestQuestion shouldHaveResponseCodeOf(String value, Response responseBody) {
		return new SearchTestQuestion(value, responseBody, ResponseProperties.STATUS_CODE);
	}
	
	public static SearchTestQuestion shouldHaveMessageOf(String value, Response responseBody) {
		return new SearchTestQuestion(value, responseBody, ResponseProperties.MESSAGE);
	}
	
	public static SearchTestQuestion shouldHaveAnErrorOf(String value, Response responseBody) {
		return new SearchTestQuestion(value, responseBody, ResponseProperties.ERROR);
	}
	
	
}
