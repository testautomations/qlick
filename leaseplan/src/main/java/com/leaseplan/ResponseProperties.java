package com.leaseplan;

public enum ResponseProperties {
	
	STATUS_CODE,
	MESSAGE,
	ERROR,
	TITLE

}
