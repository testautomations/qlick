package com.leaseplan.tasks;

import com.leaseplan.api.ApiConstants;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Get;
import net.thucydides.core.annotations.Step;

public class SearchTest implements Task{
	
	private final String product;
	
	public  SearchTest(String product) {
		this.product = product;
	}
	
	

	@Override
	@Step
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(
				Get.resource(ApiConstants.SearchApiEndPoints.searchTestProduct)
				   .with(request -> request.pathParam("product",product))
				   );				
				
		
	}
	
	public static Performable byProduct(String product) {
		return Tasks.instrumented(SearchTest.class, product);
	}
		
	 

}
