@feature
Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/test/{product} for getting the products.
### Available products: "apple", "mango", "tofu", "water"
### Prepare Positive and negative scenarios

 # Scenario:
 #   When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/apple"
 #   Then he sees the results displayed for apple
 #   When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/mango"
 #   Then he sees the results displayed for mango
 #   When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/car"
 #   Then he doesn not see the results
       
    @positive
    Scenario Outline: Positive Tests
    When he search for product <Product>
    And he should get a response of <ResponseCode> 
    Then he sees the title should have <Product>    
    
    Examples:
    |Product |ResponseCode| 
    |mango   |200         |
    |apple   |200         |
    |tofu    |200         |
    |water   |200         |
   
    
    
   
    @negative
    Scenario Outline: Negative Tests
    When he search for product <Product>  
    Then he should get a response of <ResponseCode>
    And he should get a detail error of <DetailError>
    Then he should get a detail message of <DetailMessage>
    
    Examples:
    |Product |ResponseCode|DetailError|DetailMessage|  
    |car		 |404         |true				|Not found    |
    |house	 |404         |true				|Not found    |
    
