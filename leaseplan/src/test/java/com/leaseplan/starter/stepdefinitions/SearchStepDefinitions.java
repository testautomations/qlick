package com.leaseplan.starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;


import com.leaseplan.tasks.SearchTest;
import com.leaseplan.starter.TestBase;

import static com.leaseplan.Phrases.*;
import static com.leaseplan.questions.SearchTestQuestion.*;

import static net.serenitybdd.screenplay.GivenWhenThen.*;



public class SearchStepDefinitions extends TestBase {
	
    
    @When("he search for product {word}")    
    public void searchByProduct(String product) {    	
    	givenThat(jordanKim).wasAbleTo(SearchTest.byProduct(product));    	    	
    }
    
    @Then("he sees the title should have {}")
    public void seesResultDisplayedForProduct(String product) {    	   
    	thenHe().should(seeThat(shouldHaveTitleOf(product,SerenityRest.lastResponse())));
    }
    
    
    @Then("he should get a response of {word}")
    public void heShouldGetResponseOf(String responseCode) {    	
    	thenHe().should(seeThat(shouldHaveResponseCodeOf(responseCode,SerenityRest.lastResponse())));
    }
    
    @Then("he should get a detail error of {word}")
    public void heShouldGetDetailErrorOf(String detailError) {    	
    	thenHe().should(seeThat(shouldHaveAnErrorOf(detailError,SerenityRest.lastResponse())));
    }
    
    @Then("he should get a detail message of {}")
    public void heShouldGetDetailMessageOf(String detailMessage) {    	
    	thenHe().should(seeThat(shouldHaveMessageOf(detailMessage,SerenityRest.lastResponse())));
    }


}
